#include <unistd.h>
#include "spi.h"
#include "gpio.h"
#include "ads1262.h"

#if 0// workaround for absent "ads1262.h"
class Ads1262
{
public:
    Ads1262();
    Ads1262(SPI *Spi);
    Ads1262(Gpio Data, Gpio Clk, Gpio Cs, Gpio DataIn);
    void random();
    double mesure();
    void readDetector();
    void readAllRegs();
    void LedOn();
    void LedOff();
    void blink();

private:
    int readRegister(int regAdr);
    void writeRegister(int regAdr, uint8_t val);

private:
    SPI *spi;
};
#endif

Ads1262::Ads1262() : spi{}
{
}
Ads1262::Ads1262(SPI *Spi)
{
    spi = Spi;
    printf("this constructor\n");
	
}
Ads1262::Ads1262(Gpio Data, Gpio Clk, Gpio Cs, Gpio DataIn)
{
    spi = new SPI(Data, Clk, Cs, DataIn);
	setRef();
}


int32_t Ads1262::mesure()
{
	startMeasure();
	
    usleep(100000);
	
    spi->csLow();
    spi->sendByte(0x12);
	
	recive.status = spi->reciveByte();
    recive.byte1 = spi->reciveByte();
    recive.byte2 = spi->reciveByte();
    recive.byte3 = spi->reciveByte();
    recive.byte4 = spi->reciveByte();
 
	
	spi->csHigh();
    
	
   
    return  getValFromReciveBytes();

  
}

int32_t Ads1262::getValFromReciveBytes(){
	resultFromADC = 0;
    resultFromADC  = recive.byte1 << 24;
    resultFromADC += recive.byte2 << 16;
    resultFromADC += recive.byte3 << 8;
    resultFromADC += recive.byte4;
	return resultFromADC;
}

bool Ads1262::chaeckCRC(){
	uint32_t CRC = (recive.byte1 + recive.byte2 + recive.byte3 + recive.byte4 + 0x9b) & 0xff;
    if (spi->reciveByte() != CRC){
		printf("CRC V: %x\n", spi->reciveByte());
		printf("CRCmy: V %x\n", CRC);
		printf("CRC error! \n");
		return false;
	}
	return true;
	
}
void Ads1262::readAllRegs()
{

    spi->csLow();
    spi->sendByte(0x20);
    spi->sendByte(0x1f);

    for (int i = 0; i < 26; i++)
    {
        printf("%x: %x\n", i, spi->reciveByte());
    }
    spi->csHigh();
}
void Ads1262::startMeasure(){
	spi->csLow();
    spi->sendByte(0x08);
    spi->csHigh();
}

int Ads1262::readRegister(int regAdr)
{
    spi->csLow();
    spi->sendByte(0x20 + regAdr);
    usleep(100);
    spi->sendByte(0x00);
    uint8_t val = spi->reciveByte();
    spi->csHigh();
    return val;
}
void Ads1262::writeRegister(int regAdr, uint8_t val)
{
    spi->csLow();
    spi->sendByte(0x40 + regAdr);

    spi->sendByte(0x00);
    spi->sendByte(val);
    spi->csHigh();
}
void Ads1262::setMuxReg(int muxp, int muxn){
	// printf("muxp: %d; muxn: %d\n" ,muxp ,muxn );
	uint8_t val = ((muxp & 0xf ) << 4) + (muxn & 0xf );
	// printf("set mux reg %x \n" , val);
	writeRegister(REG_INPMUX, val);
}
void Ads1262::setRef(int pos, int neg){
	
	uint8_t val = ((pos & 0x7 ) << 3) + (neg & 0x7 );
	// printf("set mux reg %x \n" , val);
	writeRegister(REG_REFMUX, val);
}
	