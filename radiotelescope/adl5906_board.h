#ifndef ADL5906_BOARD_H
#define ADL5906_BOARD_H
#include "ads1262.h"
 
class Adl5906_board : public Ads1262  {
public:
	Adl5906_board();
	// Ads1262(Gpio Data, Gpio Clk, Gpio Cs, Gpio DataIn);
	Adl5906_board(Gpio Data, Gpio Clk, Gpio Cs, Gpio DataIn) : Ads1262 ( Data,  Clk,  Cs,  DataIn) {}
	
	long double ref = 2.5 * 1000 * 1000 / 4294967296;
	
	int currentDetector;
	
	void setDetector(int ch);
	void LedOn();
	void LedOff();
	void blink();
	long double getDetectorVal(int ch);
};
#endif