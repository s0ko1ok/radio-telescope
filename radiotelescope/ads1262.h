#ifndef ADS1262_h
#define ADS1262_h
#include <unistd.h>
#include "spi.h"
#include "gpio.h"
// #include "ads1262.h"










//ch0 ain5- ain4+ 
//ch1 ain6- ain7+
//ch2 ain3- ain2+
//ch3 ain1- ain0+

#define REG_ID 				0x00
#define REG_POWER 		    0x01
#define REG_INTERFACE 	    0x02
#define REG_MODE0 		    0x03
#define REG_MODE1 		    0x04
#define REG_MODE2 		    0x05
#define REG_INPMUX 		    0x06
#define REG_OFCAL0 		    0x07
#define REG_OFCAL1 		    0x08
#define REG_OFCAL2 		    0x09
#define REG_FSCAL0 		    0x0A
#define REG_FSCAL1 		    0x0B
#define REG_FSCAL2 		    0x0C
#define REG_IDACMUX 	    0x0D
#define REG_IDACMAG 	    0x0E
#define REG_REFMUX 		    0x0F
#define REG_TDACP 		    0x10
#define REG_TDACN 		    0x11
#define REG_GPIOCON 	    0x12
#define REG_GPIODIR 	    0x13
#define REG_GPIODAT 	    0x14
#define REG_ADC2CFG 	    0x15
#define REG_ADC2MUX 	    0x16
#define REG_ADC2OFC0 	    0x17
#define REG_ADC2OFC1 	    0x18
#define REG_ADC2FSC0 	    0x19
#define REG_ADC2FSC1 	    0x1A

class Ads1262
{
public:
    Ads1262();
    Ads1262(SPI *Spi);
    Ads1262(Gpio Data, Gpio Clk, Gpio Cs, Gpio DataIn);
	
	uint8_t status;
	struct {
		uint8_t byte1;
		uint8_t byte2;
		uint8_t byte3;
		uint8_t byte4;
		uint8_t status;
	} recive;
	
	int32_t resultFromADC;
	
	int32_t getValFromReciveBytes();
    void random();
    int32_t mesure();
    void readAllRegs();

	void setMuxReg( int muxp, int muxn);

	void startMeasure();
	void writeRegister(int regAdr, uint8_t val);
	void setRef(int pos = 0, int neg = 4);
private:
    int readRegister(int regAdr);
	bool chaeckCRC();
   

private:
    SPI *spi;
};

#endif