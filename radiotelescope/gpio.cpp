#include <sys/mman.h>
#define ORANGE_PI3
#include "gpio.h"
Gpio::Gpio(){

}
Gpio::Gpio(uint32_t RegData, int BitData, uint32_t RegDir, int BitDir ):regData(RegData) // конструктор класса
    {
       regData = RegData;
        
       virtRegDataAddr = (uint32_t*)phy_adr(RegData);
   
       
        bitData = BitData;
   
       maskDir = BitDir;
       
       virtRegDirAddr = (uint32_t*)phy_adr(RegDir);
       numBitDir = (maskDir);
       setBitOutput();
    }
Gpio::Gpio(uint32_t RegData, int BitData, uint32_t RegDir, int BitDir , bool input ) // конструктор класса
    {printf("constructor #2\n");
        regData = RegData;
        virtRegDataAddr =(uint32_t*) phy_adr(RegData);
        bitData = BitData;
        maskDir = BitDir;
    
        virtRegDirAddr = (uint32_t*)phy_adr(RegDir);
        numBitDir = (maskDir);
        setBitInput();
    } 
Gpio::Gpio(uint32_t RegData, int BitData, uint32_t RegDir, int BitDir , uint32_t inputReg ) // конструктор класса
    {	printf("constructor #3\n");
        regData = RegData;
        virtRegDataAddr = (uint32_t*)phy_adr(RegData);
        bitData = BitData;
        maskDir = BitDir;
    
        virtRegDirAddr = (uint32_t*)phy_adr(RegDir);
        numBitDir = (maskDir);
       setBitOutput(); 
    }  
Gpio::Gpio(uint32_t RegData, int BitData, uint32_t RegDir, int BitDir ,  uint32_t inputReg, bool input  ) // конструктор класса
    {printf("constructor #4\n");
        regData = RegData;
		if (input)
			virtRegDataAddr = (uint32_t*)phy_adr(inputReg);
		else
			virtRegDataAddr =(uint32_t*) phy_adr(RegData);
        bitData = BitData;
        maskDir = BitDir;
    
        virtRegDirAddr = (uint32_t*)phy_adr(RegDir);
        numBitDir = (maskDir);
        setBitInput();
    }    
void Gpio::setBitHigh() {
    int reg = readData(virtRegDataAddr);
    reg = reg  | (1<< bitData);
    // printf("set bit %x\n", reg);
    writeData(virtRegDataAddr,reg );
}
void Gpio::setBitLow() {
    int reg = readData(virtRegDataAddr);
    reg = reg & ~(1<< bitData);
    writeData(virtRegDataAddr,reg );
}
void  Gpio::setBit(int val){
    if ( val ) 
        setBitHigh();
    else 
        setBitLow();    

}
void  Gpio::blink(int val){
    while(1){
		setBitHigh();
		usleep(val);
		setBitLow();
		usleep(val);
	}    

}
void Gpio::setBitOutput(){
    int reg = readData(virtRegDirAddr);
#ifdef ORANGE_PI3
	printf("orange pi 3 defined!! \n");
    reg = reg & ~(0x7 << numBitDir);
#endif
#ifdef ORANGE_PI4	
	int tmp = ~(0x1 << numBitDir);
	printBin(tmp); 
	reg = reg & tmp;
#endif
    // printf("numBitDir %x \n",numBitDir);
	printf("numBitDir !!%x\n",reg);
	printBin(reg); 
    reg += 1 << numBitDir;
	printBin(reg);
    writeData(virtRegDirAddr,reg );
}

void Gpio::setBitInput(){
    int reg = readData(virtRegDirAddr);
	printf("set input \n");
	printBin(reg);
#ifdef ORANGE_PI3
    reg = reg & ~(0x7 << numBitDir);
#endif
#ifdef ORANGE_PI4	
reg = reg & ~(0x1 << numBitDir);
#endif
	printBin(reg);
    writeData(virtRegDirAddr,reg );
}

void Gpio::setDir(bool input ){
    if (input )
        setBitInput();
    else
        setBitOutput();
}
int Gpio::getBitVal(){
    int reg = readData(virtRegDataAddr);
 
    reg = (reg >> bitData) & 0x1;
    return reg;
}
void Gpio::CycleReadBit(){
	while(1){
		int reg = readData(virtRegDataAddr);
	 
		reg = (reg >> bitData) & 0x1;
		printf("bit = %x\n",reg);
		usleep(100000);
	}
	
}
int Gpio::getBitNumber(int mask){
			int val = 0;
			if (mask == 0 )
				return 0 ;
			while((mask & 1) == 0){
				val++;
				mask = mask >> 1;
				
				if (val > 32)
					return 0;
			}
			
			return val;
		}

// void *  Gpio::phy_adr( off_t byte_addr ) {
void * Gpio::phy_adr(off_t byte_addr){
	void *map_page_addr, *map_byte_addr;
	int fd;
  // /dev/mem это файл символьного устройства, являющийся образом физической памяти.
  fd = open( "/dev/mem", O_RDWR | O_SYNC );
  if( fd < 0 ) {
    perror( "open" );
    exit( -1 ); 
  }

  // Выполняем отображение файла /dev/mem в адресное пространство нашего процесса. Получаем адрес страницы.
  map_page_addr = mmap( 0, MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, byte_addr & ~MAP_MASK );
  if( map_page_addr == MAP_FAILED ) {
    perror( "mmap" );
    exit( -1 ); 
  }

  // Вычисляем адрес требуемого слова (адрес при этом байтовый) 
  map_byte_addr = map_page_addr + (byte_addr & MAP_MASK);
	return map_byte_addr;
}
int Gpio::readData( uint32_t* address ) {
	int data = *(address);
	return data;
}
int Gpio::writeData( uint32_t* address, int data ) {
	
	*(  address ) = data;
	return 0;
}

void Gpio::printBin(int val){
	
	for (int i = 0; i< 32; i++){
		if (i%4 ==0)
			printf(" ");	
		printf("%x",(val>>(31-i)) &1);
	}
	printf("\n");
}