#include "App.h"
#include <sstream>
#include "ads1262.h"
#include "pins.h"
#include "gpio.h"
#include "adl5906_board.h"
struct DetectorResponse
{
    int32_t structID = 0x3;
    int32_t detectorVal = 0;
    char message[10] = "No error";
};
Adl5906_board * adl5906_board;
class RadioTelescopeServer
{
    struct us_listen_socket_t *global_listen_socket;

public:
    RadioTelescopeServer() = default;

    void run(int port, std::function<struct DetectorResponse()> getNextData)
    {
        /* ws->getUserData returns one of these */
        struct PerSocketData
        {
        };

        /* Very simple WebSocket broadcasting echo server */
        uWS::App()
            .ws<PerSocketData>("/*",
                               {
                                   .open = [](auto * /*ws*/) { std::cout << ".open" << std::endl; },
                                   .message = [this, getNextData](auto *ws, std::string_view message, uWS::OpCode opCode) {
                                    std::cout << ".message" << std::endl;
                                    /* Exit gracefully if we get a closedown message (ASAN debug) */
                                    if (message == "closedown")
                                    {
                                        /* Bye bye */
                                        us_listen_socket_close(0, global_listen_socket);
                                        ws->close();
                                    }

                                    // std::stringstream ss;
                                    // ss << "payload: " << getNextData();

                                    // std::cout << "Request: " << message << std::endl;
                                    // std::cout << "Response: " << ss.str() << std::endl;
                                    struct DetectorResponse data = getNextData();
                                    const std::string_view rsp((char *) &data, sizeof(data));
                                    ws->send(rsp, opCode); },
                                   .drain = [](auto * /*ws*/) {std::cout << ".drain" << std::endl;/* Check getBufferedAmount here */ },
                                   .ping = [](auto * /*ws*/, std::string_view) { std::cout << ".ping" << std::endl; },
                                   .pong = [](auto * /*ws*/, std::string_view) { std::cout << ".pong" << std::endl; },
                                   .close = [](auto * /*ws*/, int /*code*/, std::string_view /*message*/) {std::cout << ".close" << std::endl;/* We automatically unsubscribe from any topic here */ },
                               })
            .listen(port, [this](auto *listen_socket) {
                global_listen_socket = listen_socket;
                if (listen_socket)
                {
                    std::cout << "Listening on port " << 9001 << std::endl;
                }
            })
            .run();
        std::cout << "Shoot! We failed to listen and the App fell through, exiting now!" << std::endl;
    };
};

#include <random>
int main()
{	
	Gpio spi_mosi = Gpio(SPI_MOSI);
	Gpio spi_clk = Gpio(SPI_CLK);
	Gpio spi_ce = Gpio(SPI_CE0);
	Gpio spi_miso = Gpio(SPI_MISO,true);
	// Ads1262 * ads1262 = new Ads1262(spi_mosi, spi_clk, spi_ce,  spi_miso);
	adl5906_board = new Adl5906_board(spi_mosi, spi_clk, spi_ce,  spi_miso);
	
	adl5906_board -> readAllRegs();
	
	long double detectorMeasure;
	// return 1;
	// while (1){
		adl5906_board -> LedOn();
		// ads1262 -> readAllRegs();
		
		detectorMeasure = adl5906_board -> getDetectorVal(1);
		printf("detectorMeasure: %Lf \n",detectorMeasure);
		detectorMeasure = adl5906_board -> getDetectorVal(3);
		printf("detectorMeasure3: %Lf \n",detectorMeasure);
		usleep(100000);
		adl5906_board -> LedOff();
		usleep(100000);
	// }
	 
    RadioTelescopeServer server;
    server.run(9002, []() {
		
		adl5906_board -> getDetectorVal(1);
        struct DetectorResponse st = {3, adl5906_board ->resultFromADC, "success"};
        return st;
    });
}
