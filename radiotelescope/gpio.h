#ifndef GPIO_H
#define GPIO_H
#define ORANGE_PI4	
#include <iostream>
#include <unistd.h>
#include <fcntl.h>
#include <cstdint> 
#include <sys/mman.h>

#define HW_REGS_BASE ( ALT_STM_OFST )
#define HW_REGS_SPAN ( 0x04000000 )
#define HW_REGS_MASK ( HW_REGS_SPAN - 1 )
#define MAP_SIZE           (4096)
#define MAP_MASK       (MAP_SIZE-1)


class Gpio
{
private:
    bool input; 
    int regData;
    int regDir;

    uint32_t* virtRegDataAddr;
    uint32_t* virtRegDirAddr;
    int bitData;
    int maskDir;
    int numBitDir;

    int readData( uint32_t* address );
    int readData( int address );
    int writeData( uint32_t* address, int data ) ;
    int getBitNumber(int mask);
public:
    Gpio();
    Gpio(uint32_t RegData, int BitData,uint32_t RegDir, int BitDir );
    Gpio(uint32_t RegData, int BitData,uint32_t RegDir, int BitDir , bool input);
	Gpio(uint32_t RegData, int BitData,uint32_t RegDir, int BitDir , uint32_t inputReg );
	Gpio(uint32_t RegData, int BitData,uint32_t RegDir, int BitDir ,  uint32_t inputReg, bool input  );
	void setBit(int val);
    void setDir(bool input);
    void setBitHigh();
    void setBitLow();
    void setBitOutput();
    void setBitInput();
	void blink(int val  = 100000   );
    int getBitVal();
	void printBin(int val);
	void CycleReadBit();
    inline void *  phy_adr( off_t byte_addr );
}; 
#endif