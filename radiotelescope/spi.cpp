#include <unistd.h>
#include "spi.h"
#include "gpio.h"
SPI::SPI(Gpio Data,Gpio Clk, Gpio Cs, Gpio DataIn){
    dataBit= Data;
    clkBit = Clk;
    csBit=Cs;
    dataInBit= DataIn;
 }
 
void SPI::csLow() {
    csBit.setBitLow();

}		
void SPI::csHigh() {
    csBit.setBitHigh();

}
int SPI::sendByte(  int data ) {
			
			// while(1){
			for (int i = 0 ; i < 8 ;i++){
				
				// clkON();
                clkBit.setBitHigh();
                dataBit.setBit(data & 0x80);
                usleep(1);
				// putBit(data & 0x80);
				// clkOFF();
                clkBit.setBitLow();
				data = data << 1;
				 
			}
		 
			
			return 0;
		};
uint8_t SPI::reciveByte(  ) {
			uint8_t data = 0;
			int bit ; 
			for (int i = 0 ; i < 8 ;i++){
				data = data << 1;
				// clkON();
                clkBit.setBitHigh();
                usleep(1);
				bit = dataInBit.getBitVal();
				// printf("recive bit: %x \n", bit);
				data = data + bit;
				// printf("recive data: %x \n", data);
				// clkOFF();
				clkBit.setBitLow();
				 
			}
		 
		 
			// printf("reciveByte = : %x \n", data);
			return data;
		};