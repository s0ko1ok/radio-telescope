#define ORANGE_PI3	
#ifdef ORANGE_PI3 
#define reg_PD_DIR_M 0x0300B074
#define reg_PD_DIR_L 0x0300B070
#define reg_PD_DATA 0x0300B07C

#define reg_PL_DIR_M 0x0300B004
#define reg_PL_DIR_L 0x0300B000
#define reg_PL_DATA  0x0300B010

#define reg_PH_DIR_M 0x0300B100
#define reg_PH_DIR_L 0x0300B0FC
#define reg_PH_DATA  0x0300B10C


#define PD15 reg_PD_DATA, 15, reg_PD_DIR_L, 0x70000000 

#define PD16 reg_PD_DATA, 16, reg_PD_DIR_M, 0x7
#define PD18 reg_PD_DATA, 18, reg_PD_DIR_M, 0x700
#define PD21 reg_PD_DATA, 21, reg_PD_DIR_M, 0x70000
#define PD22 reg_PD_DATA, 22, reg_PD_DIR_M, 0x700000
#define PD23 reg_PD_DATA, 23, reg_PD_DIR_M, 0x7000000
#define PD24 reg_PD_DATA, 24, reg_PD_DIR_M, 0x70000000
#define PD25 reg_PD_DATA, 25, reg_PD_DIR_M, 0x700000000
#define PD26 reg_PD_DATA, 26, reg_PD_DIR_M, 0x7000000000
 

// #define PH3 reg_PH_DATA,  3, reg_PH_DIR_L, 0x7000
#define PH3 reg_PH_DATA,  3, reg_PH_DIR_L, 12
// #define PH4 reg_PH_DATA,  4, reg_PH_DIR_L, 0x70000
#define PH4 reg_PH_DATA,  4, reg_PH_DIR_L, 16
// #define PH5 reg_PH_DATA,  5, reg_PH_DIR_L, 0x700000
#define PH5 reg_PH_DATA,  5, reg_PH_DIR_L, 20
// #define PH6 reg_PH_DATA,  6, reg_PH_DIR_L, 0x7000000
#define PH6 reg_PH_DATA,  6, reg_PH_DIR_L, 24

#define PL2 reg_PL_DATA, 2, reg_PL_DIR_L, 0x700
#define PL3 reg_PL_DATA, 3, reg_PL_DIR_L, 0x7000
#define PL8 reg_PL_DATA, 8, reg_PL_DIR_M, 0x7
#define PL10 reg_PL_DATA, 10, reg_PL_DIR_M, 0x700



#define IO0 PD24
#define IO1 PD18 //ok
#define IO2 PD23
#define IO3 PL10
#define IO4 PD15 //ok
#define IO5 PD16 //ok
#define IO6 PD21

#define TWI0_SDA PD26
#define TWI0_SCK PD25

#define SPI_MOSI PH5
#define SPI_MISO PH6
#define SPI_CLK PH4
#define SPI_CE0 PH3

#define S_PWM0 PL8
#define S_PWM1 PD22 //ok

#define TXD0 PL2
#define RXD0 PL3

#endif
#ifdef ORANGE_PI4


#define GPIO1_A1 	0xFF730000, 1		,0xFF730004	,1		,0xFF730050
#define GPIO1_A3	0xFF730000,	3       ,0xFF730004  ,3      , 0xFF730050
#define GPIO1_A7    0xFF730000,	7       ,0xFF730004  ,7      , 0xFF730050
#define GPIO1_B0    0xFF730000,	8       ,0xFF730004  ,8      , 0xFF730050
#define GPIO1_B1    0xFF730000,	9       ,0xFF730004  ,9      , 0xFF730050
#define GPIO1_B2    0xFF730000,         , 0xFF730004 ,       ,  0xFF730050
#define GPIO1_C2    0xFF730000,         , 0xFF730004 ,       ,  0xFF730050
#define GPIO1_C6    0xFF730000,         , 0xFF730004 ,       ,  0xFF730050
#define GPIO1_C7    0xFF730000,         , 0xFF730004 ,       ,  0xFF730050
#define GPIO1_D0    0xFF730000,         , 0xFF730004 ,       ,  0xFF730050
#define GPIO2_D4	0xFF780000,	24+4    ,0xFF780004  ,24+4   , 0xFF780050
#define GPIO4_C0    0xFF790000,	16      ,0xFF790004  ,16     , 0xFF790050
#define GPIO4_C1	0xFF790000,	17      ,0xFF790004  ,17     , 0xFF790050
#define GPIO4_C5    0xFF790000,	21      ,0xFF790004  ,21     , 0xFF790050
#define GPIO4_C6    0xFF790000,	22      ,0xFF790004  ,22     , 0xFF790050





#endif