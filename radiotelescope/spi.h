#ifndef SPI_H
#define SPI_H
#include "gpio.h"

 
class SPI{
public:
    Gpio dataBit;
    Gpio dataInBit;
    Gpio clkBit;
    Gpio csBit;

    SPI(Gpio Data,Gpio Clk, Gpio Cs, Gpio DataIn);
   
    int sendByte(  int data ) ;
    uint8_t reciveByte(  );
    void csHigh();
    void csLow();
};
#endif