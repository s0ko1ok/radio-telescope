

class ADS1262:
	def init(self,mode):
		if mode == 'Integer Mode':
			self.ChargePumpOffset('OFF')
	
	
	registers = [
{"name":'ID'		,"val":0x00},
{"name":'POWER'	,"val"	:0x01},
{"name":'INTERFACE',"val"	:0x02},
{"name":'MODE0'	,"val"	:0x03},
{"name":'MODE1'	,"val"	:0x04},
{"name":'MODE2'	,"val"	:0x05},
{"name":'INPMUX'	,"val":0x06},
{"name":'OFCAL0'	,"val":0x07},
{"name":'OFCAL1'	,"val":0x08},
{"name":'OFCAL2'	,"val":0x09},
{"name":'FSCAL0'	,"val":0x0A},
{"name":'FSCAL1'	,"val":0x0B},
{"name":'FSCAL2'	,"val":0x0C},
{"name":'IDACMUX'	,"val":0x0D},
{"name":'IDACMAG'	,"val":0x0E},
{"name":'REFMUX'	,"val":0x0F},
{"name":'TDACP'	,"val"	:0x10},
{"name":'TDACN'	,"val"	:0x11},
{"name":'GPIOCON'	,"val":0x12},
{"name":'GPIODIR'	,"val":0x13},
{"name":'GPIODAT'	,"val":0x14},
{"name":'ADC2CFG'	,"val":0x15},
{"name":'ADC2MUX'	,"val":0x16},
{"name":'ADC2OFC0'	,"val":0x17},
{"name":'ADC2OFC1'	,"val":0x18},
{"name":'ADC2FSC0'	,"val":0x19},
{"name":'ADC2FSC1'	,"val":0x1A}
		]
		
	modes =	{'Integer Mode'
			'Fractional Mode',
			'Exact Frequency Mode',
			'requency Modulation FM Mode',
			'PM Mode',
			'Frequency Sweep Mode'			
	}
	mode = 'Integer Mode'
	digitalFilters ={
	 'Sinc1 mode' :0b000,
	 'Sinc2 mode' :0b001,
	 'Sinc3 mode' :0b010,
	 'Sinc4 mode' :0b011,
	 'FIR mode'   :0b100
	}
	def readIDRegister(self):
		val = self.readReg('ID')
		if ((val >> 5 )  & 0x7 ) == 0x1:
			self.printD("DEV_ID: ADS1263")
		if ((val >> 5 )  & 0x7 ) == 0x0:
			self.printD("DEV_ID: ADS1262")
		self.printD("REV_ID: " + str(val & 0x1f))
		
		
		
	def softRst(self):
		val = self.readReg('POWER')
		self.writeReg('POWER',val | 0x10)
		
	def setBias(self,val):
		val = self.readReg('POWER')
		if val:
			self.writeReg('POWER',val | 0x2)
		else:
			self.writeReg('POWER',val & ~0x2)	
			
	def setIntREF(self,val):
		val = self.readReg('POWER')
		if val:
			self.writeReg('POWER',val | 0x1)
		else:
			self.writeReg('POWER',val & ~0x1)
		
		
		
			
	def setTIMEOUT(self,val):
		self.setBit('INTERFACE',0x8,val)
		
	def setStatusByte(self,val):
		self.setBit('INTERFACE',0x4,val)	
		
	def setCRC(self,val):
		self.setBits('INTERFACE',0x3,val)


		
	def setReversREF(self,val):
		self.setBits('MODE0',0x80,val)	
	def setRunMode(self,val):
		self.setBit('MODE0',0x40,val)
	def setChopMode(self,val):
		self.setBits('MODE0',0x30,val)
	def setConversionDelay(self,val):
		self.setBits('MODE0',0x0f,val)
		
		
	def setDigitalFilter(self,val):
		self.setBits('MODE1',0xe0,digitalFilters[val])	
	def setSensorBiasPolarity(self,val):
		self.setBit('MODE1',0x10,val)
	
	BiasMagnitude = {
	 'No'		:0b000,
	 '0.5-mkA'	:0b001,
	 '2-mkA'		:0b010,
	 '10-mkA'	:0b011,
	 '50-mkA'	:0b100,
	 '200-mkA'	:0b101,
	 '10-MoM'	:0b110,
	}               
	def setSensorBiasMagnitude(self,val):
		self.setBits('MODE1',0x07,BiasMagnitude[val])
	
	
	def setPGABypass(self,val):
		self.setBit('MODE2',0x80,val)
		
	GainPGA ={
	 '1V/V'		: 0b000,	
	 '2V/V'		: 0b001,
	 '4V/V'		: 0b010,
	 '8V/V'		: 0b011,
	 '16V/V'	: 0b100,
	 '32V/V'	: 0b101,
	
	}	
	def setGainPGA(self,val):
		self.setBits('MODE2',0x70,GainPGA[val])	
	DataRate={
	 '2.5 SPS'		:0b0000,
	 '5 SPS'		:0b0001,
	 '10 SPS'		:0b0010,
	 '16.6SPS'		:0b0011,
	 '20 SPS'      : 0b0100,
	 '50 SPS'      : 0b0101,
	 '60 SPS'      : 0b0110,
	 '100 SPS'     : 0b0111,
	 '400 SPS'     : 0b1000,
	 '1200 SPS'    : 0b1001,
	 '2400 SPS'    : 0b1010,
	 '4800 SPS'    : 0b1011,
	 '7200 SPS'    : 0b1100,
	 '14400 SPS'  :  0b1101,
	 '19200 SPS'  :  0b1110,
	 '38400 SPS'  :  0b1111,
	}
	def setDataRate(self,val):
		self.setBits('MODE2',0x0F,DataRate[val])
		
		
	INMUX = {
	'AIN0'                    :0b0000,
	'AIN1'                    :0b0001,
	'AIN2'                    :0b0010,
	'AIN3'                    :0b0011,
	'AIN4'                    :0b0100,
	'AIN5'                    :0b0101,
	'AIN6'                    :0b0110,
	'AIN7'                    :0b0111,
	'AIN8'                    :0b1000,
	'AIN9'                    :0b1001,
	'AINCOM'                  :0b1010,
	'Temp'                    :0b1011,
	'Analog power supply'     :0b1100,
	'Digital power supply'    :0b1101,
	'TDAC'                    :0b1110,
	'Float'                   :0b1111,
	}	
	
	def setMUXP(self,val):
		self.setBits('INPMUX',0xf0,INMUX[val])
	def setMUXN(self,val):
		self.setBits('INPMUX',0x0f,INMUX[val])
		
	IDACMUX = {
		'AIN0'   :0b0000, 
		'AIN1'   :0b0001, 
		'AIN2'   :0b0010, 
		'AIN3'   :0b0011, 
		'AIN4'   :0b0100, 
		'AIN5'   :0b0101, 
		'AIN6'   :0b0110, 
		'AIN7'   :0b0111, 
		'AIN8'   :0b1000, 
		'AIN9'   :0b1001, 
		'AINCOM' :0b1010, 
		'No'     :0b1011

	}	
		
	def setIDACMUX1(self,val):
		self.setBits('IDACMUX',0xf0,IDACMUX[val])
	def setIDACMUX2(self,val):
		self.setBits('IDACMUX',0x0f,IDACMUX[val])
		
		
	IDACMAG={
	'off'      :0b0000,
	'50 mkA'    :0b0001,
	'100 mkA'   :0b0010,
	'250 mkA'   :0b0011,
	'500 mkA'   :0b0100,
	'750 mkA'   :0b0101,
	'1000 mkA'  :0b0110,
	'1500 mkA'  :0b0111,
	'2000 mkA'  :0b1000,
	'2500 mkA'  :0b1001,
	'3000 mkA'  :0b1010,
	}	
	def setIDACMAG1(self,val):
		self.setBits('IDACMAG',0xf0,IDACMAG[val])
	def setIDACMAG2(self,val):
		self.setBits('IDACMAG',0x0f,IDACMAG[val])



	REFMUX = {
	 'Internal 2.5Vref'       :0b000,
	 'External AIN1'          :0b001,
	 'External AIN3'          :0b010,
	 'External AIN5'          :0b011,
	 'Internal analog supply' :0b100,

	}
	def setREFMUX1(self,val):
		self.setBits('REFMUX',0x38,REFMUX[val])
	def setREFMUX2(self,val):
		self.setBits('REFMUX',0x07,REFMUX[val])
		
		
	def setTDACP(self,val):
		self.setBit('TDACP',0x8,val)
	def setTDACN(self,val):
		self.setBit('TDACN',0x8,val)	
		
	MAG = {
	'4.5 V'       :0b01001, 
	'3.5 V'       :0b01000, 
	'3 V'         :0b00111, 
	'2.75 V'      :0b00110, 
	'2.625 V'     :0b00101, 
	'2.5625 V'    :0b00100, 
	'2.53125 V'   :0b00011, 
	'2.515625 V'  :0b00010, 
	'2.5078125 V' :0b00001, 
	'2.5 V'       :0b00000, 
	'2.4921875 V' :0b10001, 
	'2.484375 V'  :0b10010, 
	'2.46875 V'   :0b10011, 
	'2.4375 V'    :0b10100, 
	'2.375 V'     :0b10101, 
	'2.25 V'      :0b10110, 
	'2 V'         :0b10111, 
	'1.5 V'       :0b11000, 
	'0.5 V'       :0b11001, 
	}	
	def setTDACMAGP(self,val):
		self.setBits('TDACP',0x1f,MAG[val])
	def setTDACMAGN(self,val):
		self.setBits('TDACN',0x1f,MAG[val])
		
	def setGPIO(self,val):
		self.setBits('GPIOCON',0xff,val)	
	def setGPIoDIR(self,val):
		self.setBits('GPIODIR',0xff,val)	
	def setGPIoVAL(self,val):
		self.setBits('GPIODAT',0xff,val)
		

	def writeReg(self,addr,val):
		address = self.registers[addr]
		print('read')
	
	def readReg(self,addr):
		address = self.registers[addr]
		print('read')
		return 0
		
	def printD(self, data):
		print(data)
	
	def setBit(self,reg,bit,val):
		val = self.readReg(reg)
		if val:
			self.writeReg(reg,val | bit)
		else:
			self.writeReg(reg,val & ~bit)	
	def setBits(self,reg,bit,val):
		print("todo")	
	